package mk.test.githubjobs;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import mk.test.githubjobs.model.Job;

public class JobActivity extends AppCompatActivity {
    ImageView companyLogo;
    TextView jobTitle, companyName, companyLocation, dateCreated, jobType;
    WebView howToApply, jobDescription;

    Job job;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);

        companyLogo = findViewById(R.id.company_logo);
        jobTitle = findViewById(R.id.job_title);
        companyName = findViewById(R.id.company_name);
        companyLocation = findViewById(R.id.job_location);
        dateCreated = findViewById(R.id.job_created_at);
        jobDescription = findViewById(R.id.job_description);
        howToApply = findViewById(R.id.how_to_apply);
        jobType = findViewById(R.id.job_type);

        job = getIntent().getParcelableExtra("JOB");
        jobTitle.setText(job.getTitle());
        companyName.setText(job.getCompany());
        companyLocation.setText(job.getLocation());
        dateCreated.setText(job.getCreated_at());
        jobType.setText(job.getType());

        howToApply.loadData(job.getHow_to_apply(), "text/html", "UTF-8");

        jobDescription.loadData(job.getDescription(), "text/html", "UTF-8");

        if (job.getCompany_logo() != null){
            String url = job.getCompany_logo();
            Glide
                    .with(this)
                    .load(url)
                    .placeholder(R.drawable.nothumbnail)
                    .into(companyLogo);
        }
    }

    public void closeActivity(View view) {
        finish();
    }
}
