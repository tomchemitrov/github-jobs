package mk.test.githubjobs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import mk.test.githubjobs.interfaces.CategoryClickInterface;
import mk.test.githubjobs.R;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private ArrayList<String> categories;
    private LayoutInflater inflater;
    private CategoryClickInterface categoryInterface;

    public CategoryAdapter(Context context, ArrayList<String> categories, CategoryClickInterface categoryInterface){
        this.categories = categories;
        this.inflater = LayoutInflater.from(context);
        this.categoryInterface = categoryInterface;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.category_item, parent,false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.category.setText(categories.get(position));

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{
        private TextView category;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            category = itemView.findViewById(R.id.categoryItem);

            category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (categoryInterface != null){
                        categoryInterface.onCategoryClick(category.getText().toString());
                    }
                }
            });
        }
    }
}