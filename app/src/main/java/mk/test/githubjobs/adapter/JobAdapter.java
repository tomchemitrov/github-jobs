package mk.test.githubjobs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import mk.test.githubjobs.R;
import mk.test.githubjobs.interfaces.JobClickInterface;
import mk.test.githubjobs.model.Job;

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.JobViewHolder> {

    List<Job> jobs;
    LayoutInflater inflater;
    Context context;
    JobClickInterface jobInterface;

    public JobAdapter(Context context, List<Job> jobs, JobClickInterface jobInterface){
        this.context = context;
        this.jobs = jobs;
        this.inflater = LayoutInflater.from(context);
        this.jobInterface = jobInterface;
    }

    @NonNull
    @Override
    public JobViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.job_item, parent, false);
        return new JobViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JobViewHolder holder, int position) {
        Job job = jobs.get(position);
        holder.jobTitle.setText(job.getTitle());
        holder.jobCompanyName.setText(job.getCompany());
        holder.jobCompanyLocation.setText(job.getLocation());

        if (job.getCompany_logo() != null){
            String imgUrl = job.getCompany_logo();

            Glide
                    .with(context)
                    .load(imgUrl)
                    .placeholder(R.drawable.nothumbnail)
                    //.centerCrop()
                    .into(holder.jobLogo);
        }
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }

    public class JobViewHolder extends RecyclerView.ViewHolder{

        ImageView jobLogo;
        TextView jobTitle, jobCompanyName, jobCompanyLocation;

        public JobViewHolder(@NonNull View itemView) {
            super(itemView);

            jobLogo = itemView.findViewById(R.id.company_logo);
            jobTitle = itemView.findViewById(R.id.job_title);
            jobCompanyName = itemView.findViewById(R.id.company_name);
            jobCompanyLocation = itemView.findViewById(R.id.company_location);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (jobInterface != null){
                        jobInterface.onJobClick(jobs.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
