package mk.test.githubjobs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mk.test.githubjobs.adapter.CategoryAdapter;
import mk.test.githubjobs.adapter.JobAdapter;
import mk.test.githubjobs.interfaces.CategoryClickInterface;
import mk.test.githubjobs.interfaces.JobClickInterface;
import mk.test.githubjobs.model.Job;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements CategoryClickInterface, JobClickInterface {

    EditText searchJob;
    ProgressBar progressBar;
    RecyclerView categoriesRecyclerView, jobsRecyclerView;
    ArrayList<String> categories = new ArrayList<>();
    List<Job> jobs = new ArrayList<>();
    String queryString = "";
    Gson gson;
    JobAdapter jobAdapter;
    CategoryAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        categoriesRecyclerView = findViewById(R.id.categoriesRecyclerView);
        jobsRecyclerView = findViewById(R.id.jobsRecyclerView);
        searchJob = findViewById(R.id.searchJob);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        categories.add("Android");
        categories.add("PHP");
        categories.add("Java");
        categories.add("iOS");
        categories.add("JavaScript");
        categories.add("Full Stack");

        gson = new Gson();

        jobsRecyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
        categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        categoryAdapter = new CategoryAdapter(this,categories, this);
        categoriesRecyclerView.setAdapter(categoryAdapter);

        searchJob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                progressBar.setVisibility(View.VISIBLE);
                jobsRecyclerView.setVisibility(View.INVISIBLE);
                queryString = editable.toString();
                loadJobs(queryString);
            }
        });
    }

    public void loadJobs(String query){
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://jobs.github.com/positions.json").newBuilder();
        urlBuilder.addQueryParameter("description",query);
        String url = urlBuilder.build().toString();

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    Gson gson = new Gson();
                    String jsonString = response.body().string();
                    Type listType = new TypeToken<List<Job>>(){}.getType();
                    jobs = gson.fromJson(jsonString, listType);

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           jobAdapter = new JobAdapter(MainActivity.this, jobs, MainActivity.this);
                           jobsRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                           jobsRecyclerView.setAdapter(jobAdapter);
                           progressBar.setVisibility(View.GONE);
                           jobsRecyclerView.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onCategoryClick(String text) {
        searchJob.setText(text);
        jobsRecyclerView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onJobClick(Job job) {
        Intent intent = new Intent(this, JobActivity.class);
        intent.putExtra("JOB",job);
        startActivity(intent);
    }
}