package mk.test.githubjobs.interfaces;

import mk.test.githubjobs.model.Job;

public interface JobClickInterface {
    void onJobClick(Job job);
}
