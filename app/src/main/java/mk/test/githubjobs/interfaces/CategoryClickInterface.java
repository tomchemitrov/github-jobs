package mk.test.githubjobs.interfaces;

public interface CategoryClickInterface {
    void onCategoryClick(String text);
}
