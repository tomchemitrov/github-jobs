package mk.test.githubjobs.model;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Job implements Parcelable {
    private String id, type, url, created_at;
    private String company, company_url, company_logo;
    private String title, description, how_to_apply, location;

    public Job (){}

    protected Job(Parcel in) {
        id = in.readString();
        type = in.readString();
        url = in.readString();
        created_at = in.readString();
        company = in.readString();
        company_url = in.readString();
        company_logo = in.readString();
        title = in.readString();
        description = in.readString();
        how_to_apply = in.readString();
        location = in.readString();
    }

    public static final Creator<Job> CREATOR = new Creator<Job>() {
        @Override
        public Job createFromParcel(Parcel in) {
            return new Job(in);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompany_url() {
        return company_url;
    }

    public void setCompany_url(String company_url) {
        this.company_url = company_url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHow_to_apply() {
        return how_to_apply;
    }

    public void setHow_to_apply(String how_to_apply) {
        this.how_to_apply = how_to_apply;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    public Job setItemTitle(String title){
        this.setTitle(title);
        return this;
    }

    public Job setItemCompany(String company){
        this.setCompany(company);
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(type);
        parcel.writeString(url);
        parcel.writeString(created_at);
        parcel.writeString(company);
        parcel.writeString(company_url);
        parcel.writeString(company_logo);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(how_to_apply);
        parcel.writeString(location);
    }
}
